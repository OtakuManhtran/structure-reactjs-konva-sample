# Stage 0, "build-stage", based on Node.js, to build and compile the web app
FROM node:8.12-alpine as build-stage

WORKDIR /web-app

ARG BUILD_MODE=production

# Copy source over
COPY ./ .

# NPM for web packages
RUN npm install

# Build React web
RUN npm run build:$BUILD_MODE

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15-alpine

# Copy web app to production with Nginx
COPY --from=build-stage /web-app/dist /www/app

# Copy nginx config proxy
COPY --from=build-stage /web-app/nginx/proxy.prod.conf /etc/nginx/conf.d/default.conf

WORKDIR /www

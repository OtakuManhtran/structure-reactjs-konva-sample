const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const common = require('./common.js')
const util = require('./util')
const resolve = util.resolve

const prodEnv = {
  NODE_ENV: JSON.stringify('production'),
}

const stagingEnv = {
  NODE_ENV: JSON.stringify('staging'),
}

const cssFilename_lib = 'static/css/lib.css?[hash:8]'
const cssFilename_app = 'static/css/app.css?[hash:8]'
const cssFilename_mobile = 'static/css/mobile.css?[hash:8]'
const extractCSS_LIB = new ExtractTextPlugin(cssFilename_lib)
const extractCSS_APP = new ExtractTextPlugin(cssFilename_app)
const extractCSS_MOBILE = new ExtractTextPlugin(cssFilename_mobile)

module.exports = merge(common, {
  cache: false,
  performance: {
    hints: false
  },
  output: {
    path: resolve('dist'),
    chunkFilename: 'static/js/[name].bundle.js?[hash:8]',
    filename: 'static/js/[name].js?[hash:8]',
    publicPath: '/',
  },
  stats: {
    //need it
    entrypoints: false,
    children: false,
  },
  module: {
    strictExportPresence: true, //need this
    rules: [
      {
        oneOf: [
          {
            test: /\.(png|svg|jpg|gif)$/,
            include: resolve('src'),
            loader: 'file-loader',
            options: {
              limit: 10000,
              name: '[name].[hash:8].[ext]',
              publicPath: '/static/media',
              outputPath: 'static/media',
            },
          },
          {
            test: /\.(js|jsx)$/,
            include: resolve('src'),
            loader: require.resolve('babel-loader'),
            exclude: /node_modules/,
            options: {
              plugins: [ 'react-html-attrs' ],
              compact: true,
            },
          },
          {
            test: /\.css$/,
            use: extractCSS_LIB.extract({
              fallback: 'style-loader',
              use: [ { loader: 'css-loader' } ],
            }),
          },
          {
            test: /\.scss$/,
            include: resolve('src'),
            exclude: [ /jest/, /node_modules/, /mobile\.scss$/ ],
            loader: extractCSS_APP.extract(
              Object.assign({
                fallback: require.resolve('style-loader'),
                use: [
                  {
                    loader: require.resolve('css-loader'),
                    options: {
                      importLoaders: 1,
                      minimize: true,
                      sourceMap: true,
                      publicPath: resolve('dist'),
                    },
                  },
                  {
                    loader: require.resolve('sass-loader'),
                    options: {
                      data: util.sassData
                    }
                  }
                ],
                publicPath: resolve('dist'),
              })
            ),
          },
          {
            test: /mobile\.scss$/,
            include: resolve('src'),
            exclude: [ /jest/, /node_modules/ ],
            loader: extractCSS_MOBILE.extract(
              Object.assign({
                fallback: require.resolve('style-loader'),
                use: [
                  {
                    loader: require.resolve('css-loader'),
                    options: {
                      importLoaders: 1,
                      minimize: true,
                      sourceMap: true,
                      publicPath: resolve('dist'),
                    },
                  },
                  {
                    loader: require.resolve('sass-loader')
                  }
                ],
                publicPath: resolve('dist'),
              })
            ),
          },
          {
            loader: require.resolve('file-loader'),
            exclude: [ /\.js$/, /\.html$/, /\.json$/ ],
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(resolve('dist')),
    new HtmlWebpackPlugin({
      sinject: true,
      template: resolve('public/index.html'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifySCSS: true,
        minifyURLs: true,
      },
    }),
    extractCSS_LIB,
    extractCSS_APP,
    extractCSS_MOBILE,
    new webpack.DefinePlugin({
      'process.env': process.env.NODE_ENV === 'production' ? prodEnv : stagingEnv,
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
})

const path = require('path')

module.exports = {
  resolve(relativePath) {
    return path.resolve(__dirname, '..', relativePath)
  },
  sassData: `
    @import "@styles/mixin.scss";
    @import "@styles/color";
    @import "@styles/variable";
  `,
}

const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const webpackNotifier = require('webpack-notifier')
const merge = require('webpack-merge')
const common = require('./common.js')
const util = require('./util')
const resolve = util.resolve

module.exports = merge(common, {
  cache: true, //for rebuilding faster
  output: {
    path: resolve('dev_dist'),
    filename: 'static/js/bundle.js',
    publicPath: '/',
    pathinfo: true,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [ 'babel-loader' ],
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          rulePaths: [ '/' ]
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [ 'style-loader', 'css-loader', {
          loader: 'sass-loader',
          options: {
            data: util.sassData
          }
        } ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [ 'url-loader' ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ 'es2015', 'react', 'stage-0' ],
            cacheDirectory: true,
            plugins: [ 'react-hot-loader/babel', 'react-html-attrs' ],
          },
        },
      },
      {
        test: /\.svg$/,
        use: [ 'svg-inline-loader' ],
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: 'url-loader',
            options: { name: 'image/[name]-[hash:8].[ext]' },
          },
        ],
      },
    ],
  },
  devtool: 'cheap-module-source-map',
  devServer: {
    historyApiFallback: true,
    contentBase: resolve('dev_dist'),
    port: 3005,
    host: '0.0.0.0',
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    },
    watchOptions: {
      ignored: /node_modules/,
    },
    compress: true,
    disableHostCheck: true
  },
  plugins: [
    new webpackNotifier(),
    new HtmlWebpackPlugin({
      inject: true,
      template: resolve('public/index.html'),
      minify: {
        minifyJS: false,
        minifyCSS: false,
      },
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        PLATFORM_ENV: JSON.stringify('web'),
        CR_VERSION: process.env.CR_VERSION
      },
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ]
})

import BaseService from '@app/models/BaseService'
import { apiRequest } from '@app/util'

export default class extends BaseService {
  async list({ isImage } = { isImage: false }) {
    // Mock data
    return {
      total: 25,
      list: (Array(24).fill()).map((v, i) => ({
        title: `${isImage ? 'Image' : 'Video'} ${i}`,
        src: `/assets/img/mock_${isImage ? '1' : '2'}.jpg`
      }))
    }
  }

  async converImage(info) {
    console.log('info', info)
    const file = await apiRequest({
      url: 'http://192.168.11.10:8000/photo/convert',
      method: 'post',
      data: info,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    return file
  }
}

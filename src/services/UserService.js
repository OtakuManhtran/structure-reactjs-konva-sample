import Cookies from 'js-cookie'

import BaseService from '@app/models/BaseService'
import { apiRequest } from '@app/util'
import { AUTH as STORAGE_NAME } from '@app/constant'
import config from '@app/config'

export default class extends BaseService {
  async login(username, password, persist) {
    const userRedux = this.store.getRedux('user')

    // Call API login
    // const auth = await apiRequest({
    //   path: config.api.auth.login,
    //   method: 'post',
    //   data: {
    //     username,
    //     password
    //   }
    // })
    // TODO: namhh
    // #region Mock login
    if (username !== 'admin' || password !== 'admin') {
      await this.dispatch(userRedux.actions.loginFormReset())

      throw {
        message: 'User or password invalid'
      }
    }

    const auth = {
      access_token: 'access_token',
      refresh_token: 'refresh_token',
    }

    const user = {
      email: 'admin',
      login: 'admin',
      id: 1,
      firstName: 'first',
      lastName: 'last',
    }
    // #endregion Mock login

    Cookies.set(STORAGE_NAME.AUTHORIZATION, auth.access_token)
    Cookies.set(STORAGE_NAME.AUTHORIZATION_TOKEN, auth.refresh_token)

    // const user = await apiRequest({
    //   path: config.api.user.getDetail
    // })

    await this.dispatch(userRedux.actions.loginFormReset())
    await this.dispatch(userRedux.actions.isLoginUpdate(true))
    await this.dispatch(userRedux.actions.emailUpdate(user.email))
    await this.dispatch(userRedux.actions.usernameUpdate(user.login))
    await this.dispatch(userRedux.actions.profileUpdate({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      fullName: `${user.firstName} ${user.lastName}`,
      avatar: user.imageUrl,
      email: user.email,
      username: user.login
    }))
    await this.dispatch(userRedux.actions.currentUserIdUpdate(user.id))

    if (persist) {
      // TODO: Check remember me
    }

    return {
      success: true
    }
  }

  async logout() {
    const userRedux = this.store.getRedux('user')

    apiRequest({
      path: config.api.auth.logout,
      method: 'post'
    })

    return new Promise((resolve) => {
      this.dispatch(userRedux.actions.isLoginUpdate(false))
      this.dispatch(userRedux.actions.profileReset())
      Cookies.remove(STORAGE_NAME.AUTHORIZATION)
      resolve(true)
    })
  }

  async getUserLoginInfo() {
    return await apiRequest({
      path: config.api.user.getDetail
    })
  }
}

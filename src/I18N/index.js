import _ from 'lodash'

import ja from '@app/I18N/ja'
import en from '@app/I18N/en'

const DEFAULT_LANGUAGE = 'en'
const all = _.extend({}, {
  ja,
  en
})

let lang = DEFAULT_LANGUAGE

export default {
  setLang(str) {
    if (_.includes([ 'ja', 'en' ], str)) {
      lang = str
    } else {
      throw new Error(`Invalid language: ${str}`)
    }
  },
  getLang() {
    return lang
  },
  get(key) {
    return _.get(all[lang], key, _.get(all[DEFAULT_LANGUAGE], key, key))
  }
}

export default {
  // #region Header
  'header.menu.changePassword': 'Change password',
  'header.menu.logout': 'Logout',
  'header.menu.profile': 'Profile',
  // #endregion Header

  // #region Sidebar
  'sidebar.menu.management': 'Management',
  'sidebar.menu.home': 'Home',
  'sidebar.menu.videos': 'Videos',
  'sidebar.menu.images': 'Images',
  'sidebar.menu.editor': 'Editor',
  'sidebar.menu.editVideo': 'Video',
  'sidebar.menu.editImage': 'Image',
  // #endregion Sidebar

  // #region Auth
  'login.cancel': 'Cancel',
  'login.forgot': 'Forgot password',
  'login.password': 'Password',
  'login.registerNow': 'Register now',
  'login.remember': 'Remember me',
  'login.requiredPassword': 'Please input your password',
  'login.requiredUsername': 'Please input your username',
  'login.submit': 'Login',
  'login.success': 'Login success',
  'login.title': 'Login',
  'login.username': 'Username',
  'logout.modal.confirm': 'Are you sure you want to log out?',
  // #endregion Auth

  // #region Modal
  'modal.cancel': 'Cancel',
  'modal.ok': 'Ok',
  // #endregion Modal
}

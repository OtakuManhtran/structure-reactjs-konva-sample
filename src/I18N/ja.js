export default {
  // #region Header
  'header.menu.changePassword': 'J Change password',
  'header.menu.home': 'J Home',
  'header.menu.logout': 'J Logout',
  'header.menu.profile': 'J Profile',
  // #endregion Header

  // #region Auth
  'login.cancel': 'J Cancel',
  'login.forgot': 'J Forgot password',
  'login.password': 'J Password',
  'login.registerNow': 'J Register now',
  'login.remember': 'J Remember me',
  'login.requiredPassword': 'J Please input your password',
  'login.requiredUsername': 'J Please input your username',
  'login.submit': 'J Login',
  'login.success': 'J Login success',
  'login.title': 'J Login',
  'login.username': 'J Username',
  'logout.modal.confirm': 'J Are you sure you want to log out?',
  // #endregion Auth

  // #region Modal
  'modal.cancel': 'J Cancel',
  'modal.ok': 'J Ok',
  // #endregion Modal
}

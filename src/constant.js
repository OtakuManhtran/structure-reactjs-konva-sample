export const AUTH = {
  ACCESS_TOKEN: 'access_token',
  SESSION_TOKEN: 'session_token',
  AUTHORIZATION: 'Authorization',
  AUTHORIZATION_TOKEN: 'AuthorizationToken'
}

export const MENU_KEY = {
  CHANGE_PASSWORD: 'change-password',
  HOME: 'home',
  IMAGE: 'images',
  LOGOUT: 'logout',
  PROFILE: 'profile',
  VIDEO: 'videos',
  MANAGEMENT: 'management',
  EDITOR: 'editor',
  EDITIMAGE: 'editor-image',
  EDITVIDEO: 'editor-video',

}

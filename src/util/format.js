export const numberFormat = (number) => new Intl.NumberFormat('de-DE').format(number)

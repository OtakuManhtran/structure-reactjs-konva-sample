import _ from 'lodash'
import axios from 'axios'
import Cookies from 'js-cookie'

import { AUTH } from '@app/constant'

/**
 * API Request
 * @param {*} opts
 * @param {*} external
 * @return Promise
 * apiRequest({
 *  path: 'http://example.com/users',
 *  method: 'get'
 * })
 */
export const apiRequest = (opts = {}) => {
  const headers = {}

  // let serverUrl = process.env.SERVER_URL

  opts = _.merge({
    method: 'get',
    headers,
    data: {},
    success: null,
    error: null,
    path: ''
  }, opts)

  // serverUrl += opts.path

  const method = opts.method.toLowerCase()
  const option = {
    headers: {
      'Content-Type': 'application/json',
      ...opts.headers
    },
    method: opts.method,
    // url: external ? opts.path : serverUrl
    url: opts.url || opts.path
  }

  if (method === 'post' && option.headers['Content-Type'] === 'multipart/form-data') {
    const formData = new FormData()
    _.each(opts.data, (value, key) => {
      formData.append(key, value)
    })
    option.data = formData
    option.onUploadProgress = opts.onUploadProgress

    delete option.headers['Content-Type']
  } else if (method !== 'get' && method !== 'head') {
    option.data = JSON.stringify(opts.data)
  } else {
    option.params = opts.data
  }

  return axios(option).then(res => {
    const data = res.data
    const totalCount = res.headers['x-total-count']

    if (totalCount) {
      data.totalCount = Number(totalCount)
    }

    opts.success && opts.success(data)

    return data
  }).catch(err => {
    opts.error && opts.error(err)

    return Promise.reject(err)
  })
}

export const externalRequest = async (opts = {}) => {
  return await apiRequest(opts, true)
}

/**
 * Example:
 * uploadFile(file, {
 *  error(e) {
 *    console.error(e)
 *  }
 * }).then(url => console.log(url))
 */
export const uploadFile = async (fileObject, opts = {}) => {
  try {
    const headers = {}

    const token = Cookies.get(AUTH.AUTHORIZATION)
    const refreshToken = Cookies.get(AUTH.AUTHORIZATION_TOKEN)

    if (token) {
      headers[AUTH.AUTHORIZATION] = `Bearer ${token}`
    }

    if (refreshToken) {
      headers[AUTH.AUTHORIZATION_TOKEN] = refreshToken
    }

    const option = {
      path: process.env.UPLOAD_URL,
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data',
        ...headers
      },
      data: {
        file: fileObject
      }
    }

    if (opts.onUploadProgress) {
      option.onUploadProgress = opts.onUploadProgress
    }

    const rs = await externalRequest(option)

    if (rs.access_token) {
      Cookies.set(AUTH.AUTHORIZATION, rs.access_token)
    }

    if (rs.refresh_token) {
      Cookies.set(AUTH.AUTHORIZATION_TOKEN, rs.refresh_token)
    }

    return rs.url
  } catch (err) {
    opts.error && opts.error(err)

    throw err
  }
}

import React from 'react'
import BaseComponent from '@app/models/BaseComponent'

import store from '@app/store'

export default class extends BaseComponent {
  ordRender(p) {
    return (
      <div>
        {this.ordRenderPage(p)}
      </div>
    )
  }

  ordRenderPage() {
    return null
  }

  componentDidMount() {
    const storeUser = store.getState().user

    if (!storeUser) {
      return
    }

    const isLogin = storeUser.isLogin
    const isAdmin = storeUser.isAdmin

    this.ordCheckLogin(isLogin, isAdmin)
  }

  ordCheckLogin() {
  }

  $getParam(key) {
    return key ? this.props.match.params[key] : this.props.match.params[key]
  }
}

import React from 'react'
import _ from 'lodash'

export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = _.extend({

    }, this.ordStates())

    this.$p = this.ordProps()
    this.$f = this.ordMethods()

    this.ordInit()
  }

  render() {
    return this.ordRender(this.$p)
  }

  /**
   * could be override
   * ordinary init
   * ordinary render
   * ordinary props
   * ordinary methods
   */
  ordInit() {}
  ordRender() {
    return null
  }
  ordProps() {
    return {}
  }
  ordStates() {
    return {}
  }
  ordMethods() {
    return {}
  }
}

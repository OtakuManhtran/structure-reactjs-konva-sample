import { pick } from 'lodash'

import BaseComponent from '@app/models/BaseComponent'

export default class extends BaseComponent {
  getShapeActions() {
    return {
      onDragEnd: this._handleChange,
      onTransformEnd: this._handleChange,
    }
  }

  getShapeOptions() {
    const opts = pick(this.props, [
      'x', 'y', 'url', 'base64',
      'width', 'height', 'scaleX', 'scaleY',
      'fill', 'draggable', 'dragBoundFunc',
      'name',
    ])

    return {
      x: 0, y: 0, width: 100, height: 100, scaleX: 1, scaleY: 1, draggable: true,
      ...opts
    }
  }

  // Need to override
  getShape() {
    return null
  }

  ordRender() {
    return this.getShape()
  }

  _handleChange = e => {
    const shape = e.target

    this._onTransform(shape)
  }

  _onTransform(shape) {
    this.props.onTransform({
      name: shape.name(),
      x: shape.x(),
      y: shape.y(),
      width: shape.width(),
      height: shape.height(),
      scaleX: shape.scaleX(),
      scaleY: shape.scaleY(),
      rotation: shape.rotation()
    })
  }
}

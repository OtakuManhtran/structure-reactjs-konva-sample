import _ from 'lodash'
import { constant } from '@app/util'

export default class {
  constructor() {
    this.reducers = {}
    this.actions = {}
    this.types = this.buildTypes()
    this._reducer = this.buildReducer()
    this.buildActions()
  }

  getPathName() {
    const userDefineTypes = this.defineTypes()

    if (!_.isArray(userDefineTypes) || !_.isString(userDefineTypes[0])) {
      throw new Error(`Invalid redux types definition: ${userDefineTypes}`)
    }

    return userDefineTypes[0]
  }

  buildTypes() {
    const userDefineTypes = this.defineTypes()

    if (!_.isArray(userDefineTypes) || !_.isString(userDefineTypes[0])) {
      throw new Error(`Invalid redux types definition: ${userDefineTypes}`)
    }

    const pathName = userDefineTypes[0]
    const types = {}

    _.each(this.defineDefaultState(), (value, key) => {
      // this adds a [field]Update action
      const updateKey = `${key}Update`
      types[updateKey] = `${pathName}/${key}Update`

      this.reducers[updateKey] = (state, param) => ({
        ...state,
        [key]: _.isObject(param) ? _.merge({}, state[key], param || {}) : param
      })

      this.actions[updateKey] = (param) => ({
        type: updateKey,
        payload: {
          key: param,
          pathname: pathName
        }
      })

      // this adds a [field]_reset action which resets
      // the field to the default state
      const resetKey = `${key}Reset`
      types[resetKey] = `${pathName}/${key}Reset`

      this.reducers[resetKey] = (state) => ({
        ...state,
        [key]: this.defineDefaultState()[key]
      })

      this.actions[resetKey] = () => ({
        type: resetKey
      })
    })

    return _.extend(types, constant(userDefineTypes[0], userDefineTypes[1] || []))
  }

  buildReducer() {
    const pathName = this.getPathName()

    _.extend(this.reducers, this.defineReducers())

    return (state = this.defineDefaultState(), action) => {
      const type = action.type

      if (!action.payload || action.payload.pathname === pathName) {
        if (this.types[type] && this.reducers[type]) {
          return this.reducers[type](state, _.has(action.payload, 'key') ? action.payload.key : action.payload)
        }
      }

      return state
    }
  }

  buildActions() {
    const userDefineActions = this.defineActions()
    _.extend(this.actions, userDefineActions)
  }

  getReducer() {
    return this._reducer
  }

  defineDefaultState() {
    return {}
  }

  defineTypes() {
    return []
  }

  defineActions() {
    return {}
  }

  defineReducers() {
    return {}
  }
}

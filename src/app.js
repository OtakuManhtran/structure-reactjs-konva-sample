import { map as _map, omit as _omit } from 'lodash'
import Cookies from 'js-cookie'
import React from 'react'
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import { Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

import config from '@app/config'
import { AUTH } from '@app/constant'
import store from '@app/store'
// import { apiRequest } from '@app/util'

import '@app/boot'
import '@styles/index.scss'

const middleware = (render) => {
  return render
}

const App = () => {
  const routes = _map(config.router, (item, i) => {
    const props = _omit(item, [ 'page', 'path', 'type' ])
    const R = item.type || Route
    return (
      <R path={item.path} key={i} exact component={item.page} {...props} />
    )
  })

  return (
    <div>
      <Helmet>
        <meta name="description" content="Colon-VPC" />
      </Helmet>
      <Switch id="vpc-main">
        { routes }
      </Switch>
    </div>
  )
}

const render = () => {
  ReactDOM.render(
    (
      <Provider store={store}>
        <ConnectedRouter middleware={middleware} history={store.history}>
          <App />
        </ConnectedRouter>
      </Provider>
    ),
    document.getElementById('root')
  )
}

if (Cookies.get(AUTH.AUTHORIZATION)) {
  const userRedux = store.getRedux('user')

  const data = {
    email: 'admin',
    login: 'admin',
    id: 1,
    firstName: 'first',
    lastName: 'last',
  }

  // apiRequest({
  //   path: config.api.user.getDetail,
  //   success: (data) => {
  store.dispatch(userRedux.actions.isLoginUpdate(true))

  store.dispatch(userRedux.actions.emailUpdate(data.email))
  store.dispatch(userRedux.actions.usernameUpdate(data.login))
  store.dispatch(userRedux.actions.profileUpdate({
    id: data.id,
    firstName: data.firstName,
    lastName: data.lastName,
    fullName: `${data.firstName} ${data.lastName}`,
    avatar: data.imageUrl,
    email: data.email,
    username: data.login
  }))
  store.dispatch(userRedux.actions.currentUserIdUpdate(data.id))
  store.dispatch(userRedux.actions.loadingUpdate(false))

  render()
  //   },
  //   error: () => {
  //     Cookies.remove(AUTH.AUTHORIZATION)
  //     Cookies.remove(AUTH.AUTHORIZATION_TOKEN)
  //     Cookies.remove(AUTH.ACCESS_TOKEN)
  //     Cookies.remove(AUTH.SESSION_TOKEN)
  //     render()
  //   }
  // })
} else {
  render()
}

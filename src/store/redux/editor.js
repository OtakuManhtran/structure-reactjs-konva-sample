import BaseRedux from '@app/models/BaseRedux'

class UserRedux extends BaseRedux {
  defineTypes() {
    return [ 'user' ]
  }

  defineDefaultState() {
    return {
      stage: {
        width: 640,
        height: 480
      },
      children: [ ]
    }
  }
}

export default new UserRedux()

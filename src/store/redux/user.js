import BaseRedux from '@app/models/BaseRedux'

class UserRedux extends BaseRedux {
  defineTypes() {
    return [ 'user' ]
  }

  defineDefaultState() {
    return {
      loading: false,
      isLogin: false,
      isAdmin: false,
      email: '',
      username: '',
      loginForm: {
        username: '',
        password: '',
        loading: false
      },
      registerForm: {

      },
      profile: {

      },
      currentUserId: null
    }
  }
}

export default new UserRedux()

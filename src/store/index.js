import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'

import reducer from '@app/store/reducer'

const history = createHistory({ basename: 'backend' })
const middleware = routerMiddleware(history)
const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk.withExtraArgument({})),
    applyMiddleware(middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)
store.history = history

store.history.listen(() => window.scrollTo({ left: 0, top: 0 }))

store.getRedux = (name) => {
  try {
    const redux = require(`./redux/${name}`)

    return redux.default
  } catch (err) {
    throw err
  }
}

if (process.env.NODE_ENV === 'development') {
  global._store = store
}

export default store

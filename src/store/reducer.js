import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import user from '@app/store/redux/user'
import editor from '@app/store/redux/editor'

const defaultState = {
  init: false
}

const appReducer = (state = defaultState) => {
  return state
}

export default combineReducers({
  app: appReducer,
  router: routerReducer,
  user: user.getReducer(),
  editor: editor.getReducer()
})

import React from 'react'
import { Popover, Button } from 'antd'

import BaseComponent from '@app/models/BaseComponent'

export default class extends BaseComponent {
  ordRender() {
    const { title, name, visible, onVisibleChange, children, clearPd } = this.props

    return (
      <Popover
        content={children}
        title={title}
        trigger="click"
        visible={visible}
        onVisibleChange={onVisibleChange}
        overlayClassName={clearPd ? 'popover-clear-pd' : ''}>
        <Button type="primary">{name}</Button>
      </Popover>
    )
  }
}

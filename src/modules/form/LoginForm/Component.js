import React from 'react'
import { Form, Input, Icon, Button, Checkbox } from 'antd'
import I18N from '@app/I18N'

import BaseComponent from '@app/models/BaseComponent'

import './style.scss'

const FormItem = Form.Item

class C extends BaseComponent {
  constructor(props) {
    super(props)

    this.state = {
      persist: true
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.togglePersist = this.togglePersist.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.login(values.username, values.password, this.state.persist)
      }
    })
  }

  togglePersist() {
    this.setState({
      persist: !this.state.persist
    })
  }

  getInputProps() {
    const { getFieldDecorator } = this.props.form
    const usernameFn = getFieldDecorator('username', {
      rules: [ { required: true, message: I18N.get('login.requiredUsername') } ],
      initialValue: ''
    })
    const usernameEl = (
      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
        placeholder={I18N.get('login.username')} />
    )

    const passwordFn = getFieldDecorator('password', {
      rules: [ { required: true, message: I18N.get('login.requiredPassword') } ]
    })
    const passwordEl = (
      <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
        type="password" placeholder={I18N.get('login.password')} />
    )

    const persistFn = getFieldDecorator('persist')
    const persistEl = (
      <Checkbox onClick={this.togglePersist} checked={this.state.persist} className="login-remember-me">
        {I18N.get('login.remember')}
      </Checkbox>
    )

    return {
      username: usernameFn(usernameEl),
      password: passwordFn(passwordEl),
      persist: persistFn(persistEl)
    }
  }

  ordRender() {
    const p = this.getInputProps()

    return (
      <Form onSubmit={this.handleSubmit} className="login-form form-item-clear-last">
        <FormItem>
          {p.username}
        </FormItem>
        <FormItem>
          {p.password}
        </FormItem>
        <FormItem>
          {p.persist}
          <a className="login-form__forgot pull-right" href="/login">{I18N.get('login.forgot')}</a>
          <Button loading={this.props.loading} htmlType="submit" type="primary" className="login-form__button">
            {I18N.get('login.submit')}
          </Button>

          <div className="text-center">
            <a className="login-form__register" href="/login">{I18N.get('login.registerNow')}</a>
          </div>
        </FormItem>
      </Form>
    )
  }
}

export default Form.create()(C)

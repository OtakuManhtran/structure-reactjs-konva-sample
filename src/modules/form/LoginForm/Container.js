import { message } from 'antd'

import { createContainer } from '@app/util'
import Component from './Component'
import UserService from '@app/services/UserService'
import I18N from '@app/I18N'

export default createContainer(Component, (state) => ({
  ...state.user.loginForm
}), () => {
  const userService = new UserService()

  return {
    async login(username, password, persist) {
      try {
        const rs = await userService.login(username, password, persist)

        if (rs) {
          message.success(I18N.get('login.success'))

          const loginRedirect = sessionStorage.getItem('loginRedirect')

          if (loginRedirect) {
            this.history.push(loginRedirect)
            sessionStorage.setItem('loginRedirect', null)
          } else {
            this.history.push('/')
          }
        }
      } catch (err) {
        message.error(err.message)
      }
    }
  }
})

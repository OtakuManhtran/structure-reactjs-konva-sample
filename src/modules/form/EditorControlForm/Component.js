import React from 'react'
import { Form, Input, Button, Upload, Col, Icon, Select, Switch, Radio, Row } from 'antd'

import BaseComponent from '@app/models/BaseComponent'

import './style.scss'

const RadioGroup = Radio.Group
const Option = Select.Option
const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    md: { span: 24 },
    lg: { span: 6 },
  },
  wrapperCol: {
    md: { span: 24 },
    lg: { span: 18 },
  },
}

class C extends BaseComponent {
  ordStates() {
    return {
      fileList: [],
      file: undefined
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      persist: true
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.hanlderPhotoUploadChange = this.hanlderPhotoUploadChange.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()

    this.props.form.validateFields(async (err, data) => {

      if (!err) {
        console.log('data >>>>> ', data)
        const { file } = this.state
        const info = {}

        info.photo = file
        info.size = [ data.photoWidth, data.photoHeight ]

        Object.keys(data).forEach(key => {
          if (key === 'photoWidth' || key === 'photoHeight' || key === 'photo' || !data[key]) return

          info[ key ] = data[ key ]
        })
        const demoInfo = {
          photo: info.photo,
          name: 'format',
          value: Number(info.format),
        }
        const convertResult = await this.props.converImage(demoInfo)

        if (this.props.onFileUploaded) {
          this.props.onFileUploaded({
            path: 'http://192.168.11.10:8000/photo/convert',
            imageConverted: convertResult.imageConverted
          })
        }
      }
    })
  }

  hanlderPhotoUploadChange(files) {
    const { setFieldsValue } = this.props.form
    const fileList = [ files.file ]
    const file = fileList[0]

    if (file) {
      const reader = new FileReader()
      const image = new Image()

      reader.readAsDataURL(file)
      reader.onload = () => image.src = reader.result
      image.onload = () => {
        setFieldsValue({
          photoWidth: image.width,
          photoHeight: image.height
        })

        if (this.props.onFileAdded) {
          this.props.onFileAdded({
            base64: reader.result,
            photoWidth: image.width,
          })
        }
      }
    } else {
      setFieldsValue({
        photoWidth: 0,
        photoHeight: 0
      })
    }

    this.setState({ fileList, file })
  }

  getInputProps() {
    const { fileList } = this.state
    const { getFieldDecorator } = this.props.form

    const formatFn = getFieldDecorator('format', {})
    const formatField = (
      <Select>
        <Option value="1">Jpeg</Option>
        <Option value="2">Png</Option>
        <Option value="3">Gif</Option>
        <Option value="4">SVG</Option>
      </Select>
    )

    const clippingFn = getFieldDecorator('clipping', {})
    const clippingField = (
      <RadioGroup>
        <Radio value="1">正方形</Radio>
        <Radio value="2">角丸矩形</Radio>
        <Radio value="3">円形</Radio>
      </RadioGroup>
    )

    const photoFn = getFieldDecorator('photo', {
      rules: [ { required: true, message: 'required photo' } ],
      valuePropName: 'file',
      getValueFromEvent: this.normFile,
    })
    const photoField = (
      <Upload multiple={false} accept='image/*' fileList={fileList} name="photo" showUploadList={false} beforeUpload={() => false} onChange={this.hanlderPhotoUploadChange}>
        <Button>
          <Icon type="upload" /> Click to upload
        </Button>
      </Upload>
    )

    const photoWidthFn = getFieldDecorator('photoWidth', {
      rules: [ { required: true, message: 'required photo width' } ]
    })
    const photoWidthField = (
      <Input type="number" placeholder={'Photo width'} />
    )

    const photoHeightFn = getFieldDecorator('photoHeight', {
      rules: [ { required: true, message: 'required photo height' } ]
    })
    const photoHeightField = (
      <Input type="number" placeholder={'Photo height'} />
    )

    return {
      clipping: clippingFn(clippingField),
      compress: this._getSwitchField(getFieldDecorator, 'compress'),
      format: formatFn(formatField),
      makeUp: this._getSwitchField(getFieldDecorator, 'makeUp'),
      paint: this._getSwitchField(getFieldDecorator, 'paint'),
      photo: photoFn(photoField),
      photoHeight: photoHeightFn(photoHeightField),
      photoWidth: photoWidthFn(photoWidthField),
      waterMark: this._getSwitchField(getFieldDecorator, 'waterMark'),
    }
  }

  ordRender() {
    const fields = this.getInputProps()

    return (
      <Form onSubmit={this.handleSubmit} className="form-item-clear-last">
        <Row>
          <Col xs={24} lg={12}>
            <FormItem {...formItemLayout} label="Format">
              {fields.format}
            </FormItem>
            <FormItem {...formItemLayout} label="Clipping">
              {fields.clipping}
            </FormItem>
            <FormItem {...formItemLayout} label="Photo">
              {fields.photo}
            </FormItem>
            <FormItem {...formItemLayout} label="Photo size">
              <Col span={11}>
                <FormItem>
                  {fields.photoWidth}
                </FormItem>
              </Col>
              <Col span={2} style={{ textAlign: 'center' }}>
                <span>/</span>
              </Col>
              <Col span={11}>
                <FormItem>
                  {fields.photoHeight}
                </FormItem>
              </Col>
            </FormItem>
          </Col>
          <Col xs={24} lg={12}>
            <FormItem {...formItemLayout} label="Compress">
              {fields.compress}
            </FormItem>
            <FormItem {...formItemLayout} label="Water mark">
              {fields.waterMark}
            </FormItem>
            <FormItem {...formItemLayout} label="Make up">
              {fields.makeUp}
            </FormItem>
            <FormItem {...formItemLayout} label="Paint">
              {fields.paint}
            </FormItem>
          </Col>
        </Row>
        <Row style={{ textAlign: 'right' }}>
          <Button htmlType="submit" type="primary">
            Submit
          </Button>
        </Row>
      </Form>
    )
  }

  _getSwitchField(fieldDecorator, fieldName, opts = {}) {
    const makeUpFn = fieldDecorator(fieldName, opts)
    const makeUpField = (<Switch checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="close" />} />)

    return makeUpFn(makeUpField)
  }
}

export default Form.create()(C)

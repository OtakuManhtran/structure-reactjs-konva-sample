import React from 'react'

import BasePage from '@app/models/BasePage'
import I18N from '@app/I18N'

import LoginForm from '@app/modules/form/LoginForm/Container'

import './style.scss'

export default class extends BasePage {
  ordRenderPage() {
    return (
      <div className="page-container login-page">
        <div className="login-page__logo">
          <span>
            {I18N.get('login.title')}
          </span>
        </div>

        <LoginForm />
      </div>
    )
  }

  ordCheckLogin(isLogin) {
    if (isLogin) {
      this.props.history.replace('/')
    }
  }
}

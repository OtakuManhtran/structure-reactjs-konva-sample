import React from 'react'

import StandardPage from '@app/modules/pages/StandardPage'

import './style.scss'

export default class extends StandardPage {
  ordRenderContent() {
    return (
      <div className="container">
        <h1>Home</h1>
      </div>
    )
  }
}

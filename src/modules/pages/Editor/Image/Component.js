import React from 'react'
import { Row, Col, Card, Button } from 'antd'
import CompareImage from 'react-compare-image'

import StandardPage from '@app/modules/pages/StandardPage'
import EditorControlForm from '@app/modules/form/EditorControlForm/Container'

import './style.scss'

const defaultImgSrc = '/assets/img/image-default.png'

export default class extends StandardPage {
  ordStates() {
    const image = {
      original: '',
      result: ''
    }

    return { image }
  }

  constructor(props) {
    super(props)

    this.onFileAdded = this.onFileAdded.bind(this)
    this.onFileUploaded = this.onFileUploaded.bind(this)
  }

  ordRenderContent() {
    const { image } = this.state
    console.log('this.state', this.state)
    return (
      <div>
        <Row className="editor__row">
          <EditorControlForm onFileAdded={this.onFileAdded} onFileUploaded={this.onFileUploaded} />
        </Row>
        <Row className="editor__row">
          {this.state.convertedImage ? (<a href={this.state.convertedImage}>Link ảnh</a>) : ''}
          <CompareImage style={{ border: '1px solid #ccc' }}
            leftImage={image.original || defaultImgSrc}
            rightImage={image.result || defaultImgSrc}
            handleSize={20} />
        </Row>
        <Row className="editor__compare_card">
          <Col lg={12} md={24}>
            <Card title="Original">
              <img style={{ width: '100%' }} alt="result" src={image.original || defaultImgSrc} />
            </Card>
          </Col>
          <Col lg={12} md={24}>
            <Card title="Result" extra={this._extraButtons()}>
              <img style={{ width: '100%' }} alt="result" src={image.imageConverted || defaultImgSrc} />
            </Card>
          </Col>
        </Row>
      </div>
    )
  }

  onFileUploaded(fileInfo) {
    console.log('fileInfo', fileInfo)
    const image = this.state.image
    image.result = `${fileInfo.path}/${fileInfo.imageConverted}`

    this.setState({ image, convertedImage: image.result })
  }

  onFileAdded(fileInfo) {
    const image = this.state.image
    image.original = fileInfo.base64

    this.setState({ image })
  }

  _extraButtons() {
    return (
      <Button size="small" type="primary">Clear</Button>
    )
  }
}

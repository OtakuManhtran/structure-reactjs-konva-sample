import { createContainer } from '@app/util'
// import Component from './Component'

// export default createContainer(Component, (state) => ({
//   ...state.editor
// }), () => ({}))

import ComponentPrototype from './ComponentPrototype'

export default createContainer(ComponentPrototype, (state) => ({
  ...state.editor
}), () => ({}))


import React from 'react'
import { Rect } from 'react-konva'

import BaseShape from '@app/models/BaseShape'

// Refer https://github.com/kirill3333/react-avatar
export default class extends BaseShape {
  componentDidMount() {
    console.log('TODO' + this.crop.getStage())
  }

  getShape() {
    const shapeOpts = this.getShapeOptions()
    const shapeActions = this.getShapeActions()

    return (
      <Rect ref={node => this.crop = node} {...shapeOpts} {...shapeActions}
        fill={'rgba(0, 0, 0, 0.50)'}></Rect>
    )
  }
}

import React from 'react'
import { Image } from 'react-konva'

import BaseShape from '@app/models/BaseShape'

export default class extends BaseShape {
  ordStates() {
    return {
      image: new window.Image()
    }
  }

  componentDidMount() {
    this._loadImage()
  }

  getShape() {
    const { image } = this.state
    const shapeOpts = this.getShapeOptions()
    const shapeActions = this.getShapeActions()

    return (
      <Image {...shapeActions} {...shapeOpts} image={image} ></Image>
    )
  }

  _loadImage = () => {
    const { base64, url } = this.props
    const image = new window.Image()

    image.src = base64 || url
    image.onload = () => this.setState({ image })
  }
}
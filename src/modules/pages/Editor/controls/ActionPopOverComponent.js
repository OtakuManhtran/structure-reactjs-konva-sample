import React from 'react'
import { Button, Divider, Row } from 'antd'

import BaseComponent from '@app/models/BaseComponent'
import PopOverComponent from '@app/components/PopOverComponent'

export default class extends BaseComponent {
  ordRender() {
    const { onCancel, onSave, children } = this.props

    return (
      <PopOverComponent {...this.props}>
        {
          children ? (
            <Row>
              {children}
              <Divider />
            </Row>
          ) : null
        }
        <Row style={{ padding: 10 }}>
          <Button onClick={onSave} className='editor__button' type="primary">Save</Button>
          <Button onClick={onCancel} className='editor__button' type="primary">Cancel</Button>
        </Row>
      </PopOverComponent>
    )
  }
}

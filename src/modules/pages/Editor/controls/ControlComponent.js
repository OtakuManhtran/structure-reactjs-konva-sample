import React from 'react'
import { Row, Button, Tabs, List, Card } from 'antd'

const { TabPane } = Tabs

import BaseComponent from '@app/models/BaseComponent'

import '../style.scss'

export default class extends BaseComponent {
  ordStates() {
    const data = [
      {
        title: 'Title 1',
      },
      {
        title: 'Title 2',
      },
      {
        title: 'Title 3',
      },
      {
        title: 'Title 4',
      },
      {
        title: 'Title 5',
      },  
      {
        title: 'Title 6',
      }
      ,
      {
        title: 'Title 7',
      },
      {
        title: 'Title 8',
      },
      {
        title: 'Title 9',
      }
    ]
    const itemFocus = 1
    return { data, itemFocus }
  }

  componentDidMount() {
    const { data } = this.state
    console.log('first data '+  data[0].title)
    this.setState({ itemFocus: data[0].title })
  }

  ordRender() {
    return (
      <div className="card-container">
        <Row className="editor__row">
          {this._headerButtons()}
        </Row>
        <Row>
          <Tabs ref={ref => (this.tabs = ref)} type="card" onChange={this._Callback} >
            <TabPane tab="Control" key="editor-control-tab-control">
              {this._tabControls()}
            </TabPane>
            <TabPane ref={ref => (this.tabpane = ref)} tab="Filter" key="editor-control-tab-filter">
              {this._tabFilter()}
            </TabPane>
            <TabPane tab="Image" key="editor-control-tab-image">
              <p>Content of Tab Pane 3</p>
              <p>Content of Tab Pane 3</p>
              <p>Content of Tab Pane 3</p>
            </TabPane>
          </Tabs>
        </Row>
      </div>
    )
  }

  _tabControls() {
    return (
      <div>
        {this._actionButtons()}
      </div>
    )
  }

  _Callback = (key) => {
    if(key === this.tabpane.key)
    {
      this._tabFilter()
    } 
    else
    {
      // To Do.
    }
  }

  _tabFilter() {
    return(
      <div>
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, md: 4, lg: 4, xl: 3 }}
          dataSource={this.state.data}
          renderItem={item => (
            <List.Item>
              <Card ref={ ref => this.card = ref } 
                onClick={() => this._handlerClickCard(item)} 
                onFocus = {() => this._handlerFocusCard(this.state.itemFocus)}
                style={{ alignItems: 'center' }}>
                <img ref={ ref => this.imageTemplate = ref } src={'/assets/img/anime.jpg'} style={{ width: 100, borderWidth: 1, borderStyle: 'solid', borderColor: 'red' }} />
                <p style={{ textAlign: 'center' }}>{item.title}</p>
              </Card>            
            </List.Item>
          )}
        />
      </div>
    )
  }

  _handlerClickCard = (item) => {
    console.log('EventClick item: ' + item.title)
    this.setState({ itemFocus: item.title })
  }

  _handlerFocusCard = (item) => {
    console.log('Set style item: ' + item.title)
  }

  _headerButtons() {
    const {
      handlerControlActionExport,
      handlerControlActionSave,
      handlerControlActionCancel,
      isDraft
    } = this.props
    const buttons = [
      <Button key='editor-button-export' className='editor__button' onClick={handlerControlActionExport}>Export</Button>
    ]

    if (isDraft) {
      buttons.push(
        <Button key='editor-button-save' type='primary' className='editor__button'
          onClick={handlerControlActionSave}>Save</Button>,
        <Button key='editor-button-cancel' type='primary' className='editor__button'
          onClick={handlerControlActionCancel}>Cancel</Button>
      )
    }

    return buttons
  }

  _actionButtons() {
    const {
      handlerControlActionCrop,
    } = this.props
    const buttons = []

    buttons.push(
      <Button key='editor-button-crop' type='primary' className='editor__button'
        onClick={handlerControlActionCrop}>Crop</Button>,
      <Button key='editor-button-resize' type='primary' className='editor__button'
        onClick={this._handlerResize}>Resize</Button>
    )

    return buttons
  }
}

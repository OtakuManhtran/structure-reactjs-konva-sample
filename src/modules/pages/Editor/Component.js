import React from 'react'
import { v4 } from 'uuid'
import { remove, findIndex } from 'lodash'
import { Row, Col } from 'antd'
// import { Row, Button, Input, Icon, Col } from 'antd'

import StandardPage from '@app/modules/pages/StandardPage'
import EditorComponent from '@app/modules/pages/Editor/EditorComponent'
import ControlComponent from '@app/modules/pages/Editor/controls/ControlComponent'

import './style.scss'

export default class extends StandardPage {
  ordStates() {
    // const { stage } = this.props

    const stage = {
      width: 640,
      height: 480,
      image: {
        url: '/assets/img/anime.jpg',
        width: 300,
        height: 225,
        ratio: 1
      },
      children: []
    }

    this._mathEditorStage(stage)

    return {
      selectedShapeName: '',
      isCropping: false,
      isDraft: false,
      stage
    }
  }

  ordRenderContent() {
    const { stage, isCropping, selectedShapeName } = this.state

    return (
      <div>
        <Row>
          <Col lg={24} xl={14} className="editor__row">
            <EditorComponent ref={node => this.Editor = node}
              stage={stage}
              isCropping={isCropping}
              selectedShapeName={selectedShapeName}
              handlerShapeChange={this.handlerShapeChange}
              handleStageClick={this.handleStageClick}/>
          </Col>
          <Col lg={24} xl={10}>
            <ControlComponent
              isDraft={this.state.isDraft}
              handlerControlActionSave={this.handlerControlActionSave}
              handlerControlActionCancel={this.handlerControlActionCancel}
              handlerControlActionExport ={this.handlerControlActionExport}
              handlerControlActionCrop={this.handlerControlActionCrop}/>
          </Col>
        </Row>
        {/* <Row className="editor__row">
          <Col span={6}><Input placeholder="Basic width" name={'width'} value={stage.image.width * stage.image.ratio} onChange={this.handlerOnchangeInput}/></Col>
          <Col span={6}><Input placeholder="Basic height" name={'height'}  value={stage.image.height * stage.image.ratio} onChange={this.handlerOnchangeInput}/></Col>

          <Col span={12}>
            <Button type="primary" className='editor__button' onClick={this.handlerReduction}><Icon type={'down-circle'}/></Button>
            <Button type="primary" className='editor__button' onClick={this.handlerIncrease}><Icon type={'up-circle'}/></Button>
          </Col>
        </Row> */}
      </div>
    )
  }

  handlerShapeChange = (newProps) => {
    const name = newProps.name
    const children = this.state.stage.children.concat()
    const childIndex = findIndex(children, { name })

    if (childIndex > -1) {
      children[childIndex].props = {
        ...children[childIndex].props,
        ...newProps
      }

      this.setState({ children })
    }
  }

  handleStageClick = e => {
    const { isCropping } = this.state

    if (isCropping) return

    this.setState({ selectedShapeName: e.target.name() })
  }

  // #region Control component actions

  // #region Control component tab control actions

  handlerControlActionExport = () => {
    // TODO: Handler export
  }

  handlerControlActionCrop = () => {
    const { stage } = this.state
    const name =  `crop-${v4()}`

    stage.children.push({
      type: 'crop', props: { name },
      name
    })

    this.setState({ stage, isCropping: true, isDraft: true, selectedShapeName: name })
  }

  handlerControlActionSave = () => {
    const { isCropping, stage } = this.state
    // const { $el } = this.Editor

    // TODO: get info crop to call api cropping, call api before remove crop
    if (isCropping) {
      remove(stage.children, item => item.type === 'crop')

      this.setState({ stage, isCropping: false, isDraft: false, selectedShapeName: '' })
    }
  }

  handlerControlActionCancel = () => {
    const { stage, isCropping } = this.state

    if (isCropping) {
      // Remove element in object is type crop
      remove(stage.children, item => item.type === 'crop')

      this.setState({ stage, isCropping: false, isDraft: false, selectedShapeName: '' })
    }
  }

  // #endregion Control component tab control actions

  // #endregion Control component actions

  _mathEditorStage(stage, widthInmage, heightImage) {
    stage.spaceX = (stage.width - (widthInmage || stage.image.width)) / 2
    stage.spaceY = (stage.height - (heightImage || stage.image.height)) / 2
  }

  handlerReduction = () => {
    this.changeRatio(-0.1)
  }

  handlerIncrease = () => {
    this.changeRatio(0.1)
  }

  changeRatio = (changeRatio) => {
    const { stage } = this.state

    if(changeRatio > 0) {
      stage.image.ratio =  stage.image.ratio < 10 ? stage.image.ratio + changeRatio : stage.image.ratio
    } else {
      stage.image.ratio =  stage.image.ratio > 0.5 ? stage.image.ratio + changeRatio : stage.image.ratio
    }

    this._mathEditorStage(stage, (stage.image.width * stage.image.ratio), (stage.image.height * stage.image.ratio))
    this.setState({ stage })
  }

  handlerOnchangeInput = (event) => {
    const { stage } = this.state
    const changeRatio =  event.target.name === 'width' ?  event.target.value / stage.image.width : event.target.value / stage.image.height

    stage.image.ratio = changeRatio < 10 ? changeRatio : stage.image.ratio
    this._mathEditorStage(stage, (stage.image.width * stage.image.ratio), (stage.image.height * stage.image.ratio))
    this.setState({ stage })
  }
}

import React from 'react'
import { Transformer } from 'react-konva'

import BaseComponent from '@app/models/BaseComponent'

export default class extends BaseComponent {
  componentDidMount() {
    this.checkNode()
  }

  componentDidUpdate() {
    this.checkNode()
  }

  checkNode() {
    const { selectedShapeName } = this.props
    const stage = this.transformer.getStage()
    const selectedNode = stage.findOne('.' + selectedShapeName)

    if (selectedNode === this.transformer.node()) {
      return
    }

    if (selectedNode) {
      this.transformer.attachTo(selectedNode)
    } else {
      this.transformer.detach()
    }

    this.transformer.getLayer().batchDraw()
  }

  ordRender() {
    return (
      <Transformer
        ref={node => this.transformer = node} rotateEnabled={this.props.rotate}
      />
    )
  }
}

import React from 'react'
import { map } from 'lodash'
import { Stage, Layer, Group } from 'react-konva'

import BaseComponent from '@app/models/BaseComponent'
import TransformerComponent from '@app/modules/pages/Editor/TransformerComponent'

import ImageShape from '@app/modules/pages/Editor/shapes/Image'
import CropShape from '@app/modules/pages/Editor/shapes/Crop'

import './style.scss'

export default class extends BaseComponent {
  ordRender() {
    const { handleStageClick, isCropping, selectedShapeName } = this.props
    const { children, image, width, height, spaceX, spaceY } = this.props.stage

    return (
      <Stage className="editor__konvajs" onClick={handleStageClick} ref={node => this.$el = node}
        width={width} height={height}>
        <Layer>
          <Group width={image.width} height={image.height} x={spaceX} y={spaceY} scaleY={image.ratio} scaleX={image.ratio} >
            <ImageShape key="editor-image" {...image} draggable={false} />

            {this._buildChildren(children)}

            <TransformerComponent selectedShapeName={selectedShapeName} rotate={!isCropping} />
          </Group>
        </Layer>
      </Stage>
    )
  }

  _buildChildren = (children) => {
    const { handlerShapeChange, isCropping, stage } =  this.props
    const { image, spaceX, spaceY } = stage

    return map(children, item => {
      switch (item.type) {
        case 'image':
          return <ImageShape key={item.props.name} {...item.props}
            onTransform={handlerShapeChange}
            draggable={!isCropping}></ImageShape>

        case 'crop':
          return <CropShape  
            key={item.props.name} {...item.props}
            width={image.width}
            height={image.height}
            onTransform={handlerShapeChange}
            dragBoundFunc={pos => {
              const underX = spaceX + image.width * image.ratio - (item.props.width || image.width) * image.ratio * (item.props.scaleX || 1)
              const underY = spaceY + image.height * image.ratio - (item.props.height || image.height) * image.ratio * (item.props.scaleY || 1)

              if (pos.x < spaceX) pos.x = spaceX
              else if (pos.x > underX) pos.x = underX

              if (pos.y < spaceY) pos.y = spaceY
              else if (pos.y > underY) pos.y = underY

              return pos
            }}></CropShape>
        default:
          return null
      }
    }) || null
  }
}

import React from 'react'
import { Layout } from 'antd'

import BasePage from '@app/models/BasePage'

import Sidebar from '@app/modules/pages/Layout/Sidebar/Container'
import Header from '@app/modules/pages/Layout/Header/Container'
import Footer from '@app/modules/pages/Layout/Footer/Container'

const { Content } = Layout

export default class extends BasePage {
  ordCheckLogin(isLogin) {
    if (!isLogin) {
      this.props.history.replace('/login')
    }
  }

  ordRenderPage() {
    return (
      <Layout>
        <Sidebar />
        <Layout>
          <Header />
          <Content className="standard-page">
            <div className="standard-page__content">
              {this.ordRenderContent()}
            </div>
            <Footer />
          </Content>
        </Layout>
      </Layout>
    )
  }

  ordRenderContent() {
    return null
  }
}

import { createContainer } from '@app/util'
import FileService from '@app/services/FileService'

import Component from './Component'

export default createContainer(Component, () => {
  const fileService = new FileService()

  return {
    async list() {
      return await fileService.list()
    }
  }
})

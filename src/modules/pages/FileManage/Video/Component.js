import React from 'react'
import StandardPage from '@app/modules/pages/StandardPage'

import FileList from '../List'

import '../style.scss'

export default class extends StandardPage {
  ordRenderContent() {
    const { showList }  = this.state

    return (
      <div className="container">
        <FileList title="Video" list={showList} onFilter={this.filter} />
      </div>
    )
  }

  async componentDidMount() {
    await super.componentDidMount()

    const d = await this.props.list()

    this.setState({
      total: d.total,
      list: d.list,
      showList: d.list
    })
  }

  filter = (e) => {
    console.log('test e', e)
    const updateList = this.state.list.filter((item) =>
    { return item.title.toLowerCase().indexOf(e.toLowerCase()) !==-1 })
    this.setState({ showList: updateList })
  }
}

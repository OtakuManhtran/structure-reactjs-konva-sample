import React from 'react'
import BaseComponent from '@app/models/BaseComponent'
import { List, Card, Row, Col, Input, Button } from 'antd'
import './style.scss'

export default class FileList extends BaseComponent {
  constructor(props) {
    super(props)

    this.onLoadMore = this.onLoadMore.bind(this)
  }

  ordRender() {
    const { title, list } = this.props

    return (
      <div>
        <Row>
          <Col span={12}><h1>{title}</h1></Col>
          <Col span={12}>
            <Row type="flex" justify="end">
              <Col span={8}><Input type="text" placeholder= "Input" onChange= {this.updateSearch} /></Col>
              <Col span={4} ><Button type="primary" style={{ marginLeft: 10 }} onClick={() => this.props.onFilter(this.state.value)}>Search</Button></Col>
            </Row></Col>
        </Row>
        <Row className="container">
          <Row>
            <List
              grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 4, xl: 6, xxl: 8 }}
              dataSource={list}
              renderItem={item => (
                <List.Item>
                  <Card title={item.title} className="file-manager-list__item">
                    <img src={item.src} style={{ width: '100%' }}/>
                  </Card>
                </List.Item>
              )}/>
          </Row>
          <Row style={{ textAlign: 'center' }}>
            <button type="button" className="ant-btn" onClick={this.onLoadMore}>
              <span>loading more</span>
            </button>
          </Row>
        </Row>
      </div>
    )
  }

  onLoadMore() {
    // Todo: handler load more data
  }

  updateSearch = e =>{
    this.setState({ value: e.target.value })
  }
}

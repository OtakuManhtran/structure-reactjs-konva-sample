import _ from 'lodash'
import React from 'react'
import { Layout, Menu, Icon } from 'antd'

import BaseComponent from '@app/models/BaseComponent'
import { MENU_KEY } from '@app/constant'
import I18N from '@app/I18N'

import './style.scss'

const { Sider } = Layout
const { SubMenu } = Menu
const menuKeys = [
  MENU_KEY.VIDEO,
  MENU_KEY.IMAGE,
  MENU_KEY.EDITOR,
  MENU_KEY.EDITORS,
  MENU_KEY.EDITIMAGE,
  MENU_KEY.EDITVIDEO,
]

export default class extends BaseComponent {
  constructor(props) {
    super(props)

    const keys = _.compact(props.location.pathname.split('/'))
    const key = keys.length ? keys[0] : null

    this.state = {
      menuActiveKey: keys.length === 1 ?
        key :
        keys.length > 1 ?
          keys[1] :
          MENU_KEY.HOME
    }

    this.handleMenuClick = this.handleMenuClick.bind(this)
  }

  handleMenuClick(e) {
    const key = e.key

    if (_.includes(menuKeys, key)) {
      this.props.history.push(`/${key}`)
    } else if (key === MENU_KEY.HOME) {
      this.props.history.push('/')
    }
  }

  renderMenu() {
    return (
      <Menu theme="dark" mode="inline"
        defaultSelectedKeys={[ this.state.menuActiveKey ]}
        onClick={this.handleMenuClick}>
        <Menu.Item key={MENU_KEY.HOME}>
          <Icon type="home" />
          <span>{I18N.get('sidebar.menu.home')}</span>
        </Menu.Item>
        <SubMenu key={MENU_KEY.MANAGEMENT}
          title={
            <span>
              <Icon type="setting" />
              <span>{I18N.get('sidebar.menu.management')}</span>
            </span>
          }>
          <Menu.Item key={MENU_KEY.VIDEO}>
            <Icon type="video-camera" />
            <span>{I18N.get('sidebar.menu.videos')}</span>
          </Menu.Item>
          <Menu.Item key={MENU_KEY.IMAGE}>
            <Icon type="picture" />
            <span>{I18N.get('sidebar.menu.images')}</span>
          </Menu.Item>
        </SubMenu>
        <SubMenu key={MENU_KEY.EDITORS}
          title={
            <span>
              <Icon type="edit" />
              <span>{I18N.get('sidebar.menu.editor')}</span>
            </span>
          }>
          <Menu.Item key={MENU_KEY.EDITVIDEO}>
            <Icon type="video-camera" />
            <span>{I18N.get('sidebar.menu.editVideo')}</span>
          </Menu.Item>
          <Menu.Item key={MENU_KEY.EDITIMAGE}>
            <Icon type="picture" />
            <span>{I18N.get('sidebar.menu.editImage')}</span>
          </Menu.Item>
        </SubMenu>
      </Menu>
    )
  }

  ordRender() {
    return (
      <Sider className="sidebar"
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => { console.log(broken) }}
        onCollapse={(collapsed, type) => { console.log(collapsed, type) }}>

        <div className="sidebar__logo" />

        {this.renderMenu()}
      </Sider>
    )
  }
}

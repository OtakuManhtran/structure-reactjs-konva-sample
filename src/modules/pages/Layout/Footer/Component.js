import React from 'react'

import BaseComponent from '@app/models/BaseComponent'

import './style.scss'

export default class extends BaseComponent {
  renderFooterBottom() {
    return (
      <div className="footer-bottom">
        <div className="container">
          <p className="text-center">
            &copy;2018 Colon-VPC. ALL RIGHTS RESERVED
          </p>
        </div>
      </div>
    )
  }

  ordRender() {
    return (
      <div className="footer">
        {this.renderFooterBottom()}
      </div>
    )
  }
}

import { createContainer } from '@app/util'
import Component from './Component'

export default createContainer(Component, () => ({}))

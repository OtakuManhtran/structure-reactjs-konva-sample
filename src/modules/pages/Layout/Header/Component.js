import _ from 'lodash'
import React from 'react'
import { Layout, Menu, Dropdown, Icon, Modal } from 'antd'

import BaseComponent from '@app/models/BaseComponent'
import { MENU_KEY } from '@app/constant'
import I18N from '@app/I18N'

import './style.scss'

const { Header } = Layout
const menuKeys = [
  MENU_KEY.PROFILE,
  MENU_KEY.CHANGE_PASSWORD
]

export default class extends BaseComponent {
  constructor(props) {
    super(props)

    this.handleMenuClick = this.handleMenuClick.bind(this)
  }

  handleMenuClick(e) {
    const key = e.key

    if (_.includes(menuKeys, key)) {
      this.props.history.push(`/${key}`)
    } else if (key === MENU_KEY.LOGOUT) {
      Modal.confirm({
        title: I18N.get('logout.modal.confirm'),
        content: '',
        okText: I18N.get('modal.ok'),
        cancelText: I18N.get('modal.cancel'),
        onOk: () => {
          this.props.logout()
        }
      })
    }
  }

  renderUserDropdown() {
    return (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item key={MENU_KEY.PROFILE}>{I18N.get('header.menu.profile')}</Menu.Item>
        <Menu.Item key={MENU_KEY.CHANGE_PASSWORD}>{I18N.get('header.menu.changePassword')}</Menu.Item>
        <Menu.Item key={MENU_KEY.LOGOUT}>{I18N.get('header.menu.logout')}</Menu.Item>
      </Menu>
    )
  }

  ordRender() {
    return (
      <Header className="header">
        <Dropdown overlay={this.renderUserDropdown()} className="header-user-dropdown pull-right">
          <span className="header-user-dropdown__text">
            <Icon className="header-user-dropdown__icon" type="user" /> {this.props.profile.fullName}
          </span>
        </Dropdown>
      </Header>
    )
  }
}

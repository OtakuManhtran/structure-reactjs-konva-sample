import { message } from 'antd'

import { createContainer } from '@app/util'
import Component from './Component'
import UserService from '@app/services/UserService'
import I18N from '@app/I18N'

export default createContainer(Component, (state) => ({
  isLogin: state.user.isLogin,
  profile: state.user.profile
}), () => {
  const userService = new UserService()

  return {
    async logout() {
      const rs = await userService.logout()

      if (rs) {
        message.success(I18N.get('logout.success'))

        userService.path.push('/login')
      }
    }
  }
})

import api from '@app/config/api'
import router from '@app/config/router'

export default {
  api,
  router
}

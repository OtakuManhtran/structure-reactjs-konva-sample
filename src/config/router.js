import HomePage from '@app/modules/pages/Home/Container'
import FileManageImagePage from '@app/modules/pages/FileManage/Image/Container'
import FileManageVideoPage from '@app/modules/pages/FileManage/Video/Container'
import LoginPage from '@app/modules/pages/Login/Container'
import EditImagePage from '@app/modules/pages/Editor/Image/Container'
import NotFound from '@app/modules/pages/Error/NotFound'

const routes = [
  {
    path: '/',
    page: HomePage
  },
  {
    path: '/images',
    page: FileManageImagePage
  },
  {
    path: '/videos',
    page: FileManageVideoPage
  },
  {
    path: '/editor-image',
    page: EditImagePage
  },
  {
    path: '/login',
    page: LoginPage
  }
]

function createLocalizedRoutes(routes) {
  return [
    ...routes,
    ...routes.map(r => {
      const route = { ...r }

      if (route.path) route.path = `/ja${route.path}`

      return route
    }),
    {
      page: NotFound
    }
  ]
}

export default createLocalizedRoutes(routes)

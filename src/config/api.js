export default {
  auth: {
    login: '/auth/login',
    logout: '/auth/logout'
  },
  user: {
    getDetail: '/vsuaa/api/account'
  }
}
